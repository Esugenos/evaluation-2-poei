package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class VideoGamesServiceTest {

  /*  @Test
    void shouldAddAVideoGame() {
        VideoGamesRepository repoMock = mock(VideoGamesRepository.class);
        VideoGamesService service = new VideoGamesService(repoMock);
        List<Genre> genre = new ArrayList<>();
        HashMap<Long,VideoGame> videoGamesList= new HashMap<>();
        VideoGame lapin = new VideoGame(1L, "Lapins Crétins", genre);
        when(repoMock.save(lapin)).then(videoGamesList.put(lapin.getId(),lapin));
        service.addVideoGame("Lapins Crétins");

        Assertions.assertEquals("Lapins Crétins", repoMock.get(1l).get());
    }*/

    @Test
    void shouldIHaveOneVideo() throws Exception {
        VideoGamesRepository repoMock = new VideoGamesRepository();
        RawgDatabaseClient client = new RawgDatabaseClient();
        VideoGamesService service = new VideoGamesService(repoMock,client);
        List<Genre> genre = new ArrayList<>();
        VideoGame lapin = new VideoGame(1L, "Lapins Crétins", genre,true);
        repoMock.save(lapin);

        Assertions.assertEquals("Lapins Crétins", service.getOneVideoGame(1l).getName());


    }
}


