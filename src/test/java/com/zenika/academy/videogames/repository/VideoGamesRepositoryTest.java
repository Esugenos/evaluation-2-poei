package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class VideoGamesRepositoryTest {

    @Test
    void shouldHaveAllVideoGamesOnList(){
        VideoGamesRepository repoMock = mock(VideoGamesRepository.class);
        List<Genre> genre=new ArrayList<>();
        VideoGame assassinCreed=new VideoGame(1l,"Assassin's Creed",genre,true);
        VideoGame RedDeadRedemption=new VideoGame(1l,"Red Dead Redemption",genre,true);
        HashMap<Long,VideoGame> videoGamesList= new HashMap<>();
        videoGamesList.put(1L,assassinCreed);
        videoGamesList.put(2L,RedDeadRedemption);
        when(repoMock.getAll()).thenReturn(List.copyOf(videoGamesList.values()));
        String answer="";
        for(int i=0;i<videoGamesList.size();i++){
            answer+=repoMock.getAll().get(i).getName()+"\n";
        }
          Assertions.assertEquals("Assassin's Creed\nRed Dead Redemption\n",answer
                );
    }

    @Test
    void shouldAddNewVideoGame(){
        VideoGamesRepository repo = new VideoGamesRepository();
        List<Genre> genre=new ArrayList<>();
        VideoGame assassinCreed=new VideoGame(1l,"Assassin's Creed",genre,true);
        repo.save(assassinCreed);
        repo.get(1l);
        Assertions.assertEquals("Assassin's Creed",repo.get(1l).get().getName());

    }
}
