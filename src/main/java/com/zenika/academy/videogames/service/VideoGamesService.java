package com.zenika.academy.videogames.service;

import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.repository.VideoGamesRepository;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class VideoGamesService {

    private VideoGamesRepository videoGamesRepository;
    RawgDatabaseClient client;

    @Autowired
    public VideoGamesService(VideoGamesRepository videoGamesRepository, RawgDatabaseClient client) {
        this.videoGamesRepository = videoGamesRepository;
        this.client = client;

        getDBFoundation();
    }

    public List<VideoGame> ownedVideoGames() {
        return this.videoGamesRepository.getAll();
    }

    public VideoGame getOneVideoGame(Long id) throws Exception {

        return this.videoGamesRepository.get(id).orElseThrow(() -> new Exception("Aucun jeu vidéo enregistré avec cet id"));
    }

    public VideoGame addVideoGame(String name) {

        VideoGame newGame = client.getVideoGameFromName(name);

            videoGamesRepository.save(newGame);
            return newGame;
        }
        public void deleteVideoGame(Long id){
        videoGamesRepository.delete(id);
        }
        public void updateVideoGame(Long id, Boolean update){
        videoGamesRepository.update(id, update);
        }

        public void getDBFoundation(){

        videoGamesRepository.save(client.getVideoGameFromName("Fifa"));
            videoGamesRepository.save(client.getVideoGameFromName("Sims"));
            videoGamesRepository.save(client.getVideoGameFromName("Assassin's Creed 2"));
        }

    }

