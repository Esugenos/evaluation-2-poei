package com.zenika.academy.videogames.controllers.representation;

public class VideoGameNameRepresentation {
    private String name;

    private Boolean finished;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }
}
