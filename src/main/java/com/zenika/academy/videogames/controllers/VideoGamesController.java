package com.zenika.academy.videogames.controllers;

import com.zenika.academy.videogames.controllers.representation.VideoGameNameRepresentation;
import com.zenika.academy.videogames.domain.Genre;
import com.zenika.academy.videogames.domain.VideoGame;
import com.zenika.academy.videogames.service.VideoGamesService;
import com.zenika.academy.videogames.service.rawg.RawgDatabaseClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/video-games")
public class VideoGamesController {


    private VideoGamesService videoGamesService;
    private RawgDatabaseClient client;

    @Autowired
    public VideoGamesController(VideoGamesService videoGamesService, RawgDatabaseClient client) {

        this.videoGamesService = videoGamesService;
        this.client = client;
    }

    /**
     * Récupérer la liste des jeux vidéos possédés pas l'utilisateur
     * <p>
     * Exemple :
     * <p>
     * GET /video-games
     */
    @GetMapping
  //  public List<VideoGame> listOwnedVideoGames() {
   //     return videoGamesService.ownedVideoGames();
  //  }

    /**
     * Récupérer un jeu vidéo par son ID
     * <p>
     * Exemple :
     * <p>
     * GET /video-games/3561
     */
    public List<VideoGame> getlistOwnedVideoGamesWithGenreFilter(@RequestParam(value = "genre") Genre genre) {

        final List<VideoGame> result = videoGamesService.ownedVideoGames().stream()
                .filter(result1 -> result1.getGenres().contains(genre))
                .collect(Collectors.toList());
        return result;
    }

    @GetMapping("/{id}")
    public ResponseEntity<VideoGame> getOneVideoGame(@PathVariable("id") Long id) throws Exception {
        VideoGame foundVideoGame = this.videoGamesService.getOneVideoGame(id);
        if (foundVideoGame != null) {
            return ResponseEntity.ok(foundVideoGame);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Ajouter un jeu vidéo à la collection de l'utilisateur
     * <p>
     * Exemple :
     * <p>
     * POST /video-games
     * Content-Type: application/json
     * <p>
     * {
     * "name": "The Binding of Isaac: Rebirth"
     * }
     */
    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public VideoGame addVideoGameByName(@RequestBody VideoGameNameRepresentation videoGameName) {

        if (client.getVideoGameFromName(videoGameName.getName()) != null)
            return this.videoGamesService.addVideoGame(videoGameName.getName());
        return null;
    }

    //Supprimer un jeu vidéo grâce à son id
    @DeleteMapping("/{id}")
    public String deleteVideoGame(@PathVariable(value = "id") Long id) throws Exception {
        videoGamesService.deleteVideoGame(id);
        return "Le jeu comportant l'id: " + id + " a bien été supprimé.";
    }

    @PutMapping("/{id}")
    public String updateVideoGameDone(@PathVariable(value = "id") Long id, @RequestBody VideoGameNameRepresentation update) {
        videoGamesService.updateVideoGame(id, update.getFinished());
        if (update.getFinished() == true) {
            return "Modification effectuée sur le jeu id: " + id + ", félicitations!!";
        } else {
            return "Modification effectué sur le jeu id: " + id;
        }

    }
}




