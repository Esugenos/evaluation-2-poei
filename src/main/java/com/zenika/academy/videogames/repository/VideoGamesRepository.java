package com.zenika.academy.videogames.repository;

import com.zenika.academy.videogames.domain.VideoGame;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Component
public class VideoGamesRepository {

    private HashMap<Long, VideoGame> videoGamesById = new HashMap<>();

    public List<VideoGame> getAll() {

        return List.copyOf(this.videoGamesById.values());
    }

    public Optional<VideoGame> get(Long id) {
        Optional<VideoGame> videoGame;
        videoGame = Optional.of(videoGamesById.get(id));
        return videoGame;
    }
    public void update (Long id,Boolean update){
        videoGamesById.get(id).setFinished(update);
    }

    public void save(VideoGame v) {

            videoGamesById.put(v.getId(), v);
        }
        public void delete(Long id){
        videoGamesById.remove(id);
        }
    }

